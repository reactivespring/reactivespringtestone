package com.example.R2DBCgettingstarted;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/category")
@AllArgsConstructor
public class Controller {
    @Autowired
    public CategoryRepository categoryRepository;

    @GetMapping
    public Flux<Category> getAllCategory(){
        return categoryRepository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Category>> getCategoryById(@PathVariable(value = "id") Integer id){
    return categoryRepository.findById(id). map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
    @GetMapping("/cat/{id}")
    public Mono<Category> getCategoryByIdMono(@PathVariable(value = "id") Integer id){
    return categoryRepository.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Category> saveCategory(@RequestBody Category product){
        return categoryRepository.save(product);
    }



    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Object>> deleteProduct(@PathVariable(value = "id")Integer id){
        return categoryRepository.findById(id)
                .flatMap(existingProduct->
                        categoryRepository.delete(existingProduct)
                                .then(Mono.just(ResponseEntity.ok().build()))

                ).defaultIfEmpty(ResponseEntity.notFound().build());
    }

}
