package com.example.R2DBCgettingstarted;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class R2DbCgettingstartedApplication {

	public static void main(String[] args) {
		SpringApplication.run(R2DbCgettingstartedApplication.class, args);
	}

}
